

<script>
    var db;
    var request = window.indexedDB.open("cadastro", 1);


    request.onerror = function (event) { console.log(event.log); }
    
    request.onsuccess = function (event) {
        db = request.result;
        console.log('The database is opened successfully');
    }

    
    request.onupgradeneeded = function (event) {
        db = event.target.result;
        var objectStore;
        objectStore.createIndex('name', 'name', { unique: false });
        objectStore.createIndex('email', 'email', { unique: true });

        if (!db.objectStoreNames.contains('person')) {
            objectStore = db.createObjectStore('person', { keyPath: { autoIncrement: true } });
        }
    }


/****adicionando****/
    function add() {
        var request = db.transaction(['person'], 'readwrite')
            .objectStore('person')
            .add({ id: 1, name: 'Jam', age: 24, email: 'jam@example.com' });

        request.onsuccess = function (event) {
            console.log('The data has been written successfully');
        };

        request.onerror = function (event) {
            console.log('The data has been written failed');
        }
    }

    add();
/*****************************/
















/****************lendo*****************************/
function read() {
   var transaction = db.transaction(['person']);
   var objectStore = transaction.objectStore('person');
   var request = objectStore.get(1);

   request.onerror = function(event) {
     console.log('Transaction failed');
   };

   request.onsuccess = function( event) {
      if (request.result) {
        console.log('Name: ' + request.result.name);
        console.log('Age: ' + request.result.age);
        console.log('Email: ' + request.result.email);
      } else {
        console.log('No data record');
      }
   };
}

read();
/**************************************************/









/***************************ler todos******************************/
function readAll() {
  var objectStore = db.transaction('person').objectStore('person');

   objectStore.openCursor().onsuccess = function (event) {
     var cursor = event.target.result;

     if (cursor) {
       console.log('Id: ' + cursor.key);
       console.log('Name: ' + cursor.value.name);
       console.log('Age: ' + cursor.value.age);
       console.log('Email: ' + cursor.value.email);
       cursor.continue();
    } else {
      console.log('No more data');
    }
  };
}

readAll();
/****************************************************************/




/***************************Atualizar*************************************/
function update() {
  var request = db.transaction(['person'], 'readwrite')
    .objectStore('person')
    .put({ id: 1, name: 'Jim', age: 35, email: 'Jim@example.com' });

  request.onsuccess = function (event) {
    console.log('The data has been updated successfully');
  };

  request.onerror = function (event) {
    console.log('The data has been updated failed');
  }
}

update();
/**************************************************************************/













/***************************deletar****************************************/
function remove() {
  var request = db.transaction(['person'], 'readwrite')
    .objectStore('person')
    .delete(1);

  request.onsuccess = function (event) {
    console.log('The data has been deleted successfully');
  };
}

remove();
/**************************************************************************/